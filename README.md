### What is this repository for? ###

* This repository contain test runner with possibility to create and run tests.
* 1.0

### How do I get set up? ###

* Pipeline is available, you can build this application with maven and with eclipse project builder
* To build this application use: mvn clean install
* To clone this repository use: git clone https://bitbucket.org/Kurator/tester

### Who do I talk to? ###

* Repo owner: Kuratornet (Borys Petrovskyi)
* Feel free to contact via messages or ticket system