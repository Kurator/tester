package com.kuratornet.tester.entity;

import java.io.Serializable;
import java.util.List;

public class Test implements Serializable {
	private static final long serialVersionUID = -4719759749337275602L;
	private String testName;
	private String email;
	private List<Question> questions;
	
	public Test(String testName, String email, List<Question> questions) {
		this.testName = testName;
		this.email = email;
		this.questions = questions;
	}

	public String getTestName() {
		return testName;
	}

	public String getEmail() {
		return email;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public String toString() {
		String result = "Test name: " + testName + "\n";
		result += "Test author email: " + email + "\n";
		for (int i = 0; i < questions.size(); i++) {
			result += (i+1) + ". " + questions.get(i).toString();
		}
		return result;
	}
}
