package com.kuratornet.tester.entity;

import java.io.Serializable;
import java.util.List;

public class Question implements Serializable {
	private static final long serialVersionUID = -1014445521079338033L;
	private String questionString;
	private List<String> answers;
	private int correctAnswer;
	
	public Question(String questionString, List<String> answers, int correctAnswer) {
		this.questionString = questionString;
		this.answers = answers;
		this.correctAnswer = correctAnswer;
	}

	public String getQuestionString() {
		return questionString;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public int getCorrectAnswer() {
		return correctAnswer;
	}
	
	@Override
	public String toString() {
		String result = "Question: " + questionString + "\n";
		for (int j = 0; j < answers.size(); j++) {
			result += "Answer #" + (j+1) + ": " + answers.get(j) + "\n";
		}
		return result;
	}
}
