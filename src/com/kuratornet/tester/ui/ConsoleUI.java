package com.kuratornet.tester.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.kuratornet.tester.manager.ITestManager;
import com.kuratornet.tester.manager.TestManager;

public class ConsoleUI implements UI {

	private Scanner sc;
	private ITestManager testManager;
	
	public ConsoleUI() {
		sc = new Scanner(System.in);
		testManager = new TestManager(this);
	}

	@Override
	public void startApp() {
		System.out.println("Greetings and welcome to the Tester app!");
		System.out.println("Please make your choice:");
		while (true) {
			System.out.println("1. Run test");
			System.out.println("2. Create test");
			System.out.println("3. Delete test");
			System.out.println("4. Exit");
			int userChoise = Integer.valueOf(sc.nextLine());
			switch (userChoise) {
				case 1:
					if (testManager.isTestsAvailable()) {
						if (!testManager.runTest(getChoosenTestName())) {
							System.out.println("Test is missing!!");
							break;
						}
						for (int i = 0; i < testManager.getCurrentTestQuestionsSize(); i++) {
							System.out.println((i+1) + ". " + testManager.getCurrentTestQuestion(i));
							for (int k = 0; k < 4; k++) {
								System.out.println((k+1) + ". " + testManager.getCurrentTestAnswers(i).get(k));
							}
							int answer = Integer.valueOf(sc.nextLine());
							testManager.processAnswer(i, answer);
						}
						int result = (testManager.getCorrectAnswersNumber() * 100) / testManager.getCurrentTestQuestionsSize();
						System.out.println("Your result is " + result + "%");
						break;
					} else {
						System.out.println("No tests available! Please create some tests.");
					}
					break;
				case 2:
					System.out.println("Enter test name:");
					String name = sc.nextLine();
					System.out.println("Enter user email:");
					String email = sc.nextLine();
					System.out.println("Enter amount of questions:");
					int amountOfQuestion = Integer.valueOf(sc.nextLine());
					testManager.createTest(name, email, amountOfQuestion);
					System.out.println("Test has been created succesfully!");
					continue;
				case 3:
					if (testManager.isTestsAvailable()) {
						testManager.removeTest(getChoosenTestName());
						System.out.println("Test has been deleted succesfully!");
						break;
					} else {
						System.out.println("No tests available! Please create some tests.");
					}
					break;
				case 4:
					System.exit(1);
					break;
				default:
					System.out.println("Incorrect choice, try again!");
			}
		}
	}

	@Override
	public String getQuestion(int i) {
		System.out.println("Enter your question #" + (i+1) + ":");
		return sc.nextLine();
	}

	@Override
	public List<String> getAnswers() {
		ArrayList<String> tmpAnswers = new ArrayList<String>();
		for (int j = 0; j < 3; j++) {
			System.out.println("Enter your answer #" + (j+1) + ":");
			tmpAnswers.add(sc.nextLine());
		}
		return tmpAnswers;
	}

	@Override
	public int getCorrectAnswer() {
		System.out.println("Selects correct answer number:");
		return Integer.valueOf(sc.nextLine()); 
	}
	
	private String getChoosenTestName() {
		while (true) {	
			List<String> tmpNamesList = testManager.getTestNames();
			System.out.println("Please choose test number:");
			for (int i = 0; i < tmpNamesList.size(); i++) {
				System.out.println((i + 1) + ". " + tmpNamesList.get(i));
			}
			int testChoise = Integer.valueOf(sc.nextLine());
			if (testChoise < 1 || testChoise > tmpNamesList.size()) {
				System.out.println("Incorrect number, try again!");
				continue;
			} else {
				return tmpNamesList.get(testChoise - 1);
			}
		}
	}

}
