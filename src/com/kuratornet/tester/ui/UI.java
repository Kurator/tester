package com.kuratornet.tester.ui;

import com.kuratornet.tester.manager.IManagerListener;

public interface UI extends IManagerListener {
	public void startApp();
}
