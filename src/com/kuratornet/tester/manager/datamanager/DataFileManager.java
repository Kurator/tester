package com.kuratornet.tester.manager.datamanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.kuratornet.tester.entity.Test;

public class DataFileManager implements IDataManager {
	private static final String TESTS_DIRECTORY = "tests";
	private static final String TESTS_EXTENTION = ".test";

	public DataFileManager() {
		File file = new File(TESTS_DIRECTORY);
		if (!file.exists()) {
			file.mkdir();
		} else if (!file.isDirectory()) {
			file.delete();
			file.mkdir();
		}
	}
	
	@Override
	public List<Test> getTestList() {
		ArrayList<Test> result = new ArrayList<Test>();
		File folder = new File(TESTS_DIRECTORY);
		File[] folderContent = folder.listFiles();
		for (int i = 0; i < folderContent.length; i++) {
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(folderContent[i]))) {
				result.add((Test) ois.readObject());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	@Override
	public List<String> getTestNames() {
		ArrayList<String> result = new ArrayList<String>();
		File folder = new File(TESTS_DIRECTORY);
		File[] folderContent = folder.listFiles();
		for (int i = 0; i < folderContent.length; i++) {
			result.add(folderContent[i].getName());
		}
		return result;
	}

	@Override
	public void saveTest(Test test) {
		File tmpFile = new File(TESTS_DIRECTORY + File.separator + test.getTestName() + TESTS_EXTENTION);
		try (ObjectOutputStream oouts = new ObjectOutputStream(new FileOutputStream(tmpFile))){
			oouts.writeObject(test);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void removeTest(String test) {
		File tmpFile = new File(TESTS_DIRECTORY + File.separator + test);
		if (tmpFile.exists()) {
			tmpFile.delete();
		}
	}

	@Override
	public Test getTest(String name) {
		List<Test> tests = getTestList();
		for (int i = 0; i < tests.size(); i++) {
			String testName = tests.get(i).getTestName() + TESTS_EXTENTION;
			if (testName.equals(name)) {
				return tests.get(i);
			}
		}
		return null;
	}

}
