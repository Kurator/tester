package com.kuratornet.tester.manager.datamanager;

import java.util.List;

import com.kuratornet.tester.entity.Test;

public interface IDataManager {

	List<Test> getTestList();
	List<String> getTestNames();
	void saveTest(Test test);
	void removeTest(String test);
	Test getTest(String name);

}
