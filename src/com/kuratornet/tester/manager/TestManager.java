package com.kuratornet.tester.manager;

import java.util.ArrayList;
import java.util.List;

import com.kuratornet.tester.entity.Question;
import com.kuratornet.tester.entity.Test;
import com.kuratornet.tester.manager.datamanager.DataFileManager;
import com.kuratornet.tester.manager.datamanager.IDataManager;

public class TestManager implements ITestManager {
	
	private IDataManager dataManager;
	private IManagerListener listener;
	private Test currentTest;
	private int correctAnswers;
	
	public TestManager(IManagerListener listener) {
		dataManager = new DataFileManager();
		this.listener = listener;
		dataManager.saveTest(getTestItem());
	}

	@Override
	public boolean isTestsAvailable() {
		return dataManager.getTestNames().size() > 0;
	}

	@Override
	public List<Test> getTests() {
		return dataManager.getTestList();
	}
	
	@Override
	public List<String> getTestNames() {
		return dataManager.getTestNames();
	}

	@Override
	public void createTest(String name, String email, int amountOfQuestions) {
		ArrayList<Question> list = new ArrayList<Question>();
		for (int i = 0; i < amountOfQuestions; i++) {
			list.add(new Question(listener.getQuestion(i+1), listener.getAnswers(), 
					listener.getCorrectAnswer()));
		}
		Test test = new Test(name, email, list);
		dataManager.saveTest(test);
	}

	@Override
	public void removeTest(String test) {
		dataManager.removeTest(test);
	}

	private Test getTestItem() {
		String testName = "Biography";
		String authorEmail = "kuret@ukr.net";
		List<Question> questionList = new ArrayList<Question>();
		List<String> ans1 = new ArrayList<String>();
		ans1.add("Kiev");
		ans1.add("Harkov");
		ans1.add("Odessa");
		ans1.add("Lviv");
		questionList.add(new Question("Where do you live?", ans1, 1));
		List<String> ans2 = new ArrayList<String>();
		ans2.add("Kiev");
		ans2.add("Aleksandria");
		ans2.add("Odessa");
		ans2.add("Lviv");
		questionList.add(new Question("Where you was born?", ans2, 2));
		List<String> ans3 = new ArrayList<String>();
		ans3.add("Vodka");
		ans3.add("Kompot");
		ans3.add("Rom with kola");
		ans3.add("Viski");
		questionList.add(new Question("What is your favorite drink?", ans3, 3));
		List<String> ans4 = new ArrayList<String>();
		ans4.add("WOW");
		ans4.add("Vedmak");
		ans4.add("Mario");
		ans4.add("Tetris");
		questionList.add(new Question("What is your favorite game?", ans4, 1));
		return new Test(testName, authorEmail, questionList);
	}

	@Override
	public boolean runTest(String name) {
		currentTest = dataManager.getTest(name);
		correctAnswers = 0;
		if (currentTest == null) {
			return false;
		}
		return true;
	}

	@Override
	public int getCurrentTestQuestionsSize() {
		return currentTest.getQuestions().size();
	}

	@Override
	public String getCurrentTestQuestion(int i) {
		return currentTest.getQuestions().get(i).getQuestionString();
	}

	@Override
	public List<String> getCurrentTestAnswers(int i) {
		return currentTest.getQuestions().get(i).getAnswers();
	}

	@Override
	public void processAnswer(int questionNumber, int answerNumber) {
		if (answerNumber == currentTest.getQuestions().get(questionNumber).getCorrectAnswer()) {
			correctAnswers++;
		}
	}

	@Override
	public int getCorrectAnswersNumber() {
		return correctAnswers;
	}
}
