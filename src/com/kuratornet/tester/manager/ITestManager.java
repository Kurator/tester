package com.kuratornet.tester.manager;

import java.util.List;

import com.kuratornet.tester.entity.Test;

public interface ITestManager {

	boolean isTestsAvailable();
	List<Test> getTests();
	void createTest(String name, String email, int amountOfQuestions);
	void removeTest(String testName);
	List<String> getTestNames();
	boolean runTest(String string);
	int getCurrentTestQuestionsSize();
	String getCurrentTestQuestion(int i);
	List<String> getCurrentTestAnswers(int i);
	void processAnswer(int questionNumber, int answer);
	int getCorrectAnswersNumber();

}
