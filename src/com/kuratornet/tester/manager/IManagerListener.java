package com.kuratornet.tester.manager;

import java.util.List;

public interface IManagerListener {
	String getQuestion(int i);
	List<String> getAnswers();
	int getCorrectAnswer();
}
