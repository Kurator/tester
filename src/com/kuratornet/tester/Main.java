package com.kuratornet.tester;

import com.kuratornet.tester.ui.ConsoleUI;
import com.kuratornet.tester.ui.UI;

public class Main {
	public static void main(String[] args) {
		try {
			UI ui = new ConsoleUI();
			ui.startApp();
			// test string
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
